package me.thewoosh.anticheat.check.move;

import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.plugintestutilities.mocks.FakePlayer;
import org.bukkit.Location;
import org.junit.Test;
import static org.junit.Assert.*;

public class FlyCheckTest {

    private final FlyCheck instance = new FlyCheck();
    private final FakePlayer player = new FakePlayer();
    private final PlayerData playerData = new PlayerData(player);

    @Test
    public void noMoveTest() {
        assertNull("FlyCheck failed for move event without change of movement", instance.runCheck(playerData, new MovementData(player.getLocation(), player.getLocation().clone(), true, true)));
    }

    @Test
    public void jumpTest() {
        Location loc = player.getLocation().clone();
        loc.add(0, 0.42, 0);

        assertNull("FlyCheck failed for move event with jump as action", instance.runCheck(playerData, new MovementData(player.getLocation(), loc, true, true)));
    }

}
