/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import me.thewoosh.anticheat.commands.CommandAntiCheat;
import me.thewoosh.anticheat.data.UpdateDispatcher;
import me.thewoosh.anticheat.event.DebugListener;
import me.thewoosh.anticheat.event.EventListener;
import me.thewoosh.anticheat.event.InventoryListener;
import me.thewoosh.anticheat.event.packet.PlayerPacketListener;
import me.thewoosh.anticheat.manage.CheckManager;
import me.thewoosh.anticheat.manage.PlayerManager;
import me.thewoosh.anticheat.packetlisteners.BoatMove;
import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class PluginCore extends JavaPlugin {

    private final CheckManager checkManager = new CheckManager(getLogger());
    private final PlayerManager playerManager = new PlayerManager();
    private final UpdateDispatcher updateDispatcher = new UpdateDispatcher(checkManager);

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new EventListener(checkManager, getLogger(), playerManager), this);
        Bukkit.getPluginManager().registerEvents(new DebugListener(getLogger(), playerManager), this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(checkManager, playerManager), this);

        // ProtocolLib hooks
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(new BoatMove(this));
        protocolManager.addPacketListener(new PlayerPacketListener(this));

        CommandAntiCheat commandAntiCheat = new CommandAntiCheat(this.playerManager);
        PluginCommand pluginCommand = Objects.requireNonNull(getCommand("ac"));
        pluginCommand.setExecutor(commandAntiCheat);
        pluginCommand.setTabCompleter(commandAntiCheat);
    }

    @NotNull
    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    @NotNull
    public UpdateDispatcher getUpdateDispatcher() {
        return updateDispatcher;
    }
}
