/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check;

import org.jetbrains.annotations.NotNull;

public abstract class Check {

    private final Category category;
    private final String name;

    private boolean enabled = true;

    public Check(@NotNull Category category,
                 @NotNull String name) {
        this.category = category;
        this.name = name;
    }

    @NotNull
    public Category getCategory() {
        return category;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @NotNull
    public String getName() {
        return name;
    }
}
