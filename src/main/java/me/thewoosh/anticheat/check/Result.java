package me.thewoosh.anticheat.check;

import org.jetbrains.annotations.NotNull;

/**
 * TODO: Find better name, since it isn't really a result, it's more about
 *       information on why the check failed.
 *
 */
public class Result {

    private final String subCheckName;
    private final String description;
    private final String[] verboseInformation;

    public Result(@NotNull final String subCheckName,
                  @NotNull final String description,
                  @NotNull final String...verboseInformation) {
        this.subCheckName = subCheckName;
        this.description = description;
        this.verboseInformation = verboseInformation;
    }

    @NotNull
    public String getSubCheckName() {
        return subCheckName;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    @NotNull
    public String[] getVerboseInformation() {
        return verboseInformation;
    }

}
