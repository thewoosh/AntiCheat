package me.thewoosh.anticheat.check;

import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.data.RotationMotionData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface AccurateRotationCheck {

    /**
     * Runs the check.
     *
     * @param player The player which performed the rotation.
     * @param movement The information collected about the rotation.
     *
     * @return null if the player passed, non-null if failed.
     */
    @Nullable
    Result runCheck(@NotNull PlayerData player,
                    @NotNull RotationMotionData rotationData);

}
