package me.thewoosh.anticheat.check.move;

import me.thewoosh.anticheat.check.Category;
import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.MoveCheck;
import me.thewoosh.anticheat.check.Result;
import me.thewoosh.anticheat.data.InventoryData;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import org.jetbrains.annotations.NotNull;

public class InventoryMoveCheck extends Check implements MoveCheck {

    public InventoryMoveCheck() {
        super(Category.MOVEMENT, "InventoryMove");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {
        final InventoryData inventoryData = player.getInventoryData();

        if (inventoryData.isOpen()) {
            if (player.getPlayer().isSprinting() && !inventoryData.checkDelay()) {
                return new Result("Sprinting",
                        "The player can't sprint when they have an inventory open.",
                        "withMotivation=" + player.getInventoryData().getOpenMotivation()
                );
            }
            if (player.getPlayer().isSneaking() && !inventoryData.checkDelay()) {
                return new Result("Sneaking",
                        "The player can't sneak when they have an inventory open.",
                        "withMotivation=" + player.getInventoryData().getOpenMotivation()
                );
            }
        }

        return null;
    }

}
