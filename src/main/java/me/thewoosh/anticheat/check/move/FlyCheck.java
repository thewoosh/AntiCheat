/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check.move;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.interactions.BlockInteractions;
import me.thewoosh.anticheat.interactions.ClimbInteraction;
import me.thewoosh.anticheat.interactions.MathHelper;
import org.bukkit.Bukkit;
import org.bukkit.block.BlockFace;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;

public class FlyCheck extends Check implements MoveCheck {

    private static final double STEP_HEIGHT = 0.5D;

    public FlyCheck() {
        super(Category.MOVEMENT, "Fly");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {

        final double jumpPower = calculateJumpPower(player);
        final boolean hasCorrectJumpPower = MathHelper.compareDoubles(jumpPower, movement.getDeltaY());

        ClimbInteraction lowerClimbInteraction = BlockInteractions.findLatterInteraction(movement.getFrom(), BlockFace.SELF);
        final double ladderMotion = 0.1176;
        final boolean hasLadderMotion = lowerClimbInteraction != ClimbInteraction.NOT && MathHelper.compareDoubles(movement.getDeltaY(), ladderMotion);

        if (movement.wasOnGround() && !movement.isOnGround() && movement.getDeltaY() > 0) {
            if (!hasCorrectJumpPower && !hasLadderMotion) {
                return new Result("JumpPower",
                        "When the player jumps, the first motion is constant, but might be affected by a jump potion.",
                        Information.doubleComparison(jumpPower, movement.getDeltaY()),
                        Information.withPotion(PotionEffectType.JUMP, player.getPlayer().getPotionEffect(PotionEffectType.JUMP))
                );
            }
        }

        if (!movement.wasOnGround() && !movement.isOnGround() && lowerClimbInteraction != ClimbInteraction.NOT && movement.getDeltaY() > 0 && !hasLadderMotion) {
            return new Result("LadderMotion",
                    "Check for correct motion when on ladder",
                    Information.doubleComparison(ladderMotion, movement.getDeltaY()),
                    Information.withPotion(PotionEffectType.JUMP, player.getPlayer().getPotionEffect(PotionEffectType.JUMP))
            );
        }

        if (movement.wasOnGround() && movement.isOnGround() && movement.getDeltaY() > 0) {
//            double expectedOffset = BlockInteractions.calculatePossibleSlabMovement(movement.getFrom(), movement.getTo());
            BlockInteractions.HighestResult resultFrom = BlockInteractions.calculateHeightAt(movement.getFrom());
            BlockInteractions.HighestResult resultTo = BlockInteractions.calculateHeightAt(movement.getTo());

            if (resultFrom.block == null || resultTo.block == null) {
                Bukkit.broadcastMessage("HighestLoc> No block candidate could be selected.");
            } else {
                double delta = resultTo.height - resultFrom.height;
                Bukkit.broadcastMessage("motionY=" + movement.getDeltaY());
//                Bukkit.broadcastMessage("StepCheck(From) %s %s".formatted(Information.withCoordinates(movement.getFrom()), resultFrom.toString()));
//                Bukkit.broadcastMessage("StepCheck(To) %s %s".formatted(Information.withCoordinates(movement.getTo()), resultTo.toString()));

                if (delta == 0) {
                    return new Result("NonSteppable",
                            "Player can't step on this block",
                            "withMotionY=%.5f".formatted(movement.getDeltaY()),
                            // Doesn't matter if resultFrom or resultTo is selected, they're the same
                            "withBlock=%s %s".formatted(resultFrom.block.getType(), Information.withCoordinates(resultFrom.block.getLocation()))
                    );
                }
                if (delta > STEP_HEIGHT) {
                    return new Result("HighStep",
                            "Player can step onto blocks if the deltaY <= 0.5",
                            "withDelta=" + delta + " > 0.5",
                            "highestFrom=" + resultFrom.toString(),
                            "highestTo=" + resultTo.toString()
                    );
                }
                if (!MathHelper.compareDoubles(movement.getDeltaY(), delta)) {
                    return new Result("IncorrectStep",
                            "Player can step onto blocks if the deltaY == toY-fromY",
                            Information.doubleComparison(delta, movement.getDeltaY()),
                            "highestFrom=" + resultFrom.toString(),
                            "highestTo=" + resultTo.toString()
                    );
                }
            }
        }

        if (!movement.wasOnGround() && !movement.isOnGround()) {
            final double expectedDeltaY = (player.getLastMovementData().getDeltaY() - 0.08) * 0.98;
            if (Math.abs(expectedDeltaY) >= 0.008D && !MathHelper.compareDoubles(movement.getDeltaY(), expectedDeltaY)) {
                return new Result("Gravity",
                        "Check for gravity on falling, jumping etc.",
                        Information.doubleComparison(movement.getDeltaY(), expectedDeltaY)
                );
            }
        }

        /*System.out.println("var_dump:" +
                " hasLadderMotion: " + hasLadderMotion +
                " hasCorrectJumpPower: " + hasCorrectJumpPower +
                " onGround: " + movement.wasOnGround() +
                " wasOnGround: "  + movement.wasOnGround() +
                " deltaY: " + movement.getDeltaY()
        );
         */

        return null;
    }

    private double calculateJumpPower(@NotNull final PlayerData playerData) {
        PotionEffect effect = playerData.getPlayer().getPotionEffect(PotionEffectType.JUMP);
        double result = 0.42;

        if (effect != null) {
            result += (effect.getAmplifier() + 1) * 0.1;
        }

        return result;
    }

}
