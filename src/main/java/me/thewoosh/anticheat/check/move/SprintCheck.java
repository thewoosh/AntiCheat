package me.thewoosh.anticheat.check.move;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.data.RotationMotionData;
import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SprintCheck extends Check implements MoveCheck {

    /**
     * If sprinting and rotating the yaw a lot, the v can be higher than the
     * threshold. This yaw delta threshold accounts for that.
     */
    private static final double THRESHOLD_JANK_SPRINTING = 1.0;
    private static final double THRESHOLD_RELATION_INDEX = 1.0;

    public SprintCheck() {
        super(Category.MOVEMENT, "Sprint");
    }

    @Override
    public @Nullable Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {
        Location location = movement.getTo();
        Vector direction = location.getDirection();
        Vector distance = movement.getTo().toVector().subtract(movement.getFrom().toVector()).normalize();

        RotationMotionData rotationMotionData = new RotationMotionData(
                movement.getFrom().getPitch(),
                movement.getFrom().getYaw(),
                movement.getTo().getPitch(),
                movement.getTo().getYaw()
        );

        /*
         * The relation index is the numberic value of the relation between the
         * rotational direction and the localized direction, which is basically
         * a magic value that represents the difference between the movement
         * vector and the direction vector.
         *
         * This means roughly:
         * index <= 1 the player can sprint
         * otherwise  the player can't sprint
         */
        final double relationIndex = pythagoras(distance.clone().subtract(direction));

        if (!Double.isNaN(relationIndex) &&
                relationIndex > THRESHOLD_RELATION_INDEX &&
                Math.abs(rotationMotionData.deltaYaw) < THRESHOLD_JANK_SPRINTING &&
                player.getPlayer().isSprinting() &&
                player.wasOnGround()
        ) {
            return new Result("Direction",
                    "The player can only sprint if they go forward",
                    "withThreshold(ri) %.5f > %.5f".formatted(relationIndex, THRESHOLD_RELATION_INDEX),
                    "withThreshold(jank) %.5f < %.5f".formatted(Math.abs(rotationMotionData.deltaYaw), THRESHOLD_JANK_SPRINTING),
                    "withSprinting=" + player.getPlayer().isSprinting(),
                    "withOnGround=" + player.wasOnGround()
            );
        }

        return null;
    }

    private double pythagoras(Vector relation) {
        return Math.sqrt(
                relation.getX() * relation.getX() +
                relation.getZ() * relation.getZ()
        );
    }
}
