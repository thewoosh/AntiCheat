package me.thewoosh.anticheat.check.move;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

import static me.thewoosh.anticheat.interactions.BlockCollections.*;

public class NoFallCheck extends Check implements MoveCheck {

    public NoFallCheck() {
        super(Category.MOVEMENT, "NoFall");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {
        Collection<Block> surrounding = getBlocksSurrounding(movement.getTo());
        if (getIncorrectBlock(surrounding, m -> m.getType() == Material.AIR) != null) {
            return null;
        }

        if (!movement.isOnGround() && movement.wasOnGround()) {
            return null; //todo
        }

        Location location = movement.getTo().clone();
        location.add(0, -0.0001, 0);
        Collection<Block> below = getBlocksSurrounding(location);

        Block notFullBlock = getIncorrectBlock(below, m -> !m.getType().isOccluding());
        Block fullBlock = getIncorrectBlock(below, m -> !m.getType().isOccluding());
        boolean oneNotFullBlock = notFullBlock == null;
        boolean oneFullBlock = fullBlock != null;

        if (movement.isOnGround() && oneNotFullBlock) {
            return new Result("OnGround", "Player isn't actually on ground",
                    "from=" + Information.withCoordinates(movement.getFrom()),
                    "to=" + Information.withCoordinates(movement.getTo()),
                    "cl=" + Information.withRawCoordinates(location),
                    "blocks=" + notFullBlock + " " + fullBlock,
//                    "yes=" + (movement.getTo().getY() - notFullBlock.getY()),
                    "below=" + printBlockCollection("Below", below),
                    "bools=nfb=%b fb=%b to.onGround=%b from.onGround=%b".formatted(oneNotFullBlock, oneFullBlock, movement.isOnGround(), movement.wasOnGround())
            );
        }

        if (!movement.isOnGround() && oneFullBlock) {
            return new Result("InAir", "Player isn't actually in air");
        }

        return null;
    }

}
