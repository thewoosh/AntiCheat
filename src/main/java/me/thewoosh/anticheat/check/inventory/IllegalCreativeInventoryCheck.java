package me.thewoosh.anticheat.check.inventory;

import me.thewoosh.anticheat.check.Category;
import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.InventoryCheck;
import me.thewoosh.anticheat.check.Result;
import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.event.inventory.InventoryEvent;
import org.jetbrains.annotations.NotNull;

public class IllegalCreativeInventoryCheck extends Check implements InventoryCheck {

    public IllegalCreativeInventoryCheck() {
        super(Category.INVENTORY, "IllegalCreativeInventory");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull InventoryEvent inEvent) {
//        if (!(inEvent instanceof InventoryCreativeEvent)) {
//            return null;
//        }
//
//        InventoryCreativeEvent event = (InventoryCreativeEvent) inEvent;
//        Bukkit.broadcastMessage("Cursor=" + event.getCursor().hashCode() + " " + event.getCurrentItem().hashCode());

        return null;
    }
}
