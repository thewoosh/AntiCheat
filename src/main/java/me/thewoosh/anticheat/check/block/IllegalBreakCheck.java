package me.thewoosh.anticheat.check.block;

import me.thewoosh.anticheat.check.BlockBreakCheck;
import me.thewoosh.anticheat.check.Category;
import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.Result;
import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;

public class IllegalBreakCheck extends Check implements BlockBreakCheck {

    public IllegalBreakCheck() {
        super(Category.BLOCK, "IllegalBreak");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player,
                           @NotNull BlockBreakEvent event) {
        Material material = event.getBlock().getType();
        Bukkit.broadcastMessage("BlockBreak: " + event.getBlock().getType().name());

        if (isLiquid(material)) {
            return new Result("Liquid",
                    "Players can't break liquid blocks.",
                    "withMaterial=" + material.name()
            );
        }

        if (material.isAir()) {
            return new Result("Air",
                    "Players can't break air blocks.",
                    "withMaterial=" + material.name()
            );
        }

        return null;
    }

    private boolean isLiquid(Material material) {
        return material == Material.WATER || material == Material.LAVA;
    }

}
