package me.thewoosh.anticheat.check.block;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.DataTarget;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.data.RotationData;
import me.thewoosh.anticheat.interactions.MathHelper;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class ScaffoldCheck extends Check implements MoveCheck, BlockPlaceCheck {

    private final short repetitionThreshold = 4;

    public ScaffoldCheck() {
        super(Category.BLOCK, "Scaffold");
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {
        final boolean yawChanged = !MathHelper.compareDoubles(movement.getTo().getYaw(), movement.getFrom().getYaw());
        final boolean pitchChanged = !MathHelper.compareDoubles(movement.getTo().getPitch(), movement.getFrom().getPitch());

        final RotationData rotationData = player.getRotationData(DataTarget.AURA);
        if (yawChanged || pitchChanged) {
            RotationData.RotationEntry entry = rotationData.addEntry(movement.getTo().getPitch(), movement.getTo().getYaw());

//            Bukkit.broadcastMessage("§8[§6§lAC§8] §7Rot changed: y=§6%f §7p=§6%f §7(from y=§6%f §7p=§6%f§7)".formatted(
//                    movement.getTo().getYaw(), movement.getTo().getPitch(),
//                    movement.getFrom().getYaw(), movement.getFrom().getPitch()
//            ));

            if (!rotationData.isReady()) {
                return null;
            }

            final RotationData.RotationEntry[] entries = rotationData.getHistory();
            short repetitions = 0;

            for (int i = 1; i < entries.length; i++) {
                if (entries[i].elevatedEquals(entries[0])) {
                    repetitions++;
                }
            }

            if (repetitions >= repetitionThreshold) {
                return new Result("RepeatingRotation",
                        "The players rotation is repeating artificially.",
                        Information.withThreshold(this.repetitionThreshold, repetitions)
                );
            }
        }

        return null;
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull BlockPlaceEvent event) {
        Vector deltaVector = player.getPlayer().getLocation().toVector().subtract(event.getBlockPlaced().getLocation().toVector());

        BlockFace face = getRelation(event.getBlockPlaced(), event.getBlockAgainst());
        if (isBlockFaceHorizontalFour(face) && deltaVector.getY() >= 1.0) {
            if ((face == BlockFace.EAST && deltaVector.getX() <= 0) ||
                (face == BlockFace.WEST && deltaVector.getX() >= 1) ||
                (face == BlockFace.SOUTH && deltaVector.getZ() <= 0) ||
                (face == BlockFace.NORTH && deltaVector.getZ() >= 1)) {
                return new Result("Far",
                        "Player is placing too far relative to block below",
                        "withFace=" + face,
                        "withDelta=" + deltaVector);
            }
        }

        return null;
    }

    private boolean isBlockFaceHorizontalFour(BlockFace blockFace) {
        return blockFace == BlockFace.NORTH || blockFace == BlockFace.EAST || blockFace == BlockFace.SOUTH || blockFace == BlockFace.WEST;
    }

    private BlockFace getRelation(Block a, Block b) {
        int x = a.getX() - b.getX();
        int y = a.getY() - b.getY();
        int z = a.getZ() - b.getZ();

        for (BlockFace face : BlockFace.values()) {
            if (face.getModX() == x && face.getModY() == y && face.getModZ() == z) {
                return face;
            }
        }

        return null;
    }

}
