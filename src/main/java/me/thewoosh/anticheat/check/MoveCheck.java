/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check;

import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface MoveCheck {

    /**
     * Runs the check.
     *
     * @param player The player which performed the movement.
     * @param movement The information collected about the movement.
     *
     * @return null if the player passed, non-null if failed.
     */
    @Nullable
    Result runCheck(@NotNull PlayerData player,
                    @NotNull MovementData movement);

}
