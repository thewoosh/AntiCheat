package me.thewoosh.anticheat.check;

import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.event.inventory.InventoryEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface InventoryCheck {

    /**
     * Runs the check.
     *
     * @param player The player involved in the check.
     * @param event The event that occurred.
     *
     * @return null if the player passed, non-null if failed.
     */
    @Nullable
    Result runCheck(@NotNull PlayerData player,
                    @NotNull InventoryEvent event);

}
