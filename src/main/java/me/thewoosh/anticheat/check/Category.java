/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check;

import org.jetbrains.annotations.NotNull;

/**
 * Category defines the category of which the Check belongs to.
 */
public enum Category {

    BLOCK,
    COMBAT,
    INVENTORY,
    MOVEMENT,
    ;

    private final String printableName;

    Category() {
        char[] letters = name().toCharArray();
        for (int i = 0; i < letters.length; i++) {
            letters[i] = (i == 0) ? Character.toUpperCase(letters[i]) : Character.toLowerCase(letters[i]);
        }
        printableName = new String(letters);
    }

    @NotNull
    public String getPrintableName() {
        return printableName;
    }
}
