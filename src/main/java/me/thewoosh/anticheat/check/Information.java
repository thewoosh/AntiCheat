package me.thewoosh.anticheat.check;

import org.bukkit.Location;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Information {

    @NotNull
    public static String doubleComparison(double a, double b) {
        return "compare (" + a + " isn't " + b + ")";
    }

    @NotNull
    public static String withPotion(@NotNull PotionEffectType type, @Nullable PotionEffect effect) {
        final String prefix = "with potion of type " + type.getName();

        if (effect == null) {
            return prefix + " is null";
        }

        return prefix + " with amplifier level " + (effect.getAmplifier() + 1);
    }

    @NotNull
    public static String withCoordinates(@NotNull Location location) {
        return "(%.3f, %.3f, %.3f)".formatted(location.getX(), location.getY(), location.getZ());
    }

    @NotNull
    public static String withRawCoordinates(@NotNull Location location) {
        return "(%f, %f, %f)".formatted(location.getX(), location.getY(), location.getZ());
    }

    @NotNull
    public static String withThreshold(long threshold, long value) {
        return "threshold(%d >= %d)".formatted(value, threshold);
    }

}
