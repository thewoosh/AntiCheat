package me.thewoosh.anticheat.check.combat;

import me.thewoosh.anticheat.check.Category;
import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.CombatCheck;
import me.thewoosh.anticheat.data.CombatData;
import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Vehicle;
import org.jetbrains.annotations.NotNull;

public class BoatCombat extends Check implements CombatCheck {

    public BoatCombat() {
        super(Category.COMBAT, "BoatCombat");
    }

    @Override
    public boolean runCheck(@NotNull PlayerData player, @NotNull CombatData combat) {
        Vehicle vehicle = (Vehicle) player.getPlayer().getVehicle();

        if (!(vehicle instanceof Boat)) {
            return true;
        }

        // Drivers cannot attack entities when the paddles are moving
        boolean isDriver = vehicle.getPassengers().get(0) == player.getPlayer();
        boolean paddlesMoving = player.getBoatData().leftPaddleMoving || player.getBoatData().rightPaddleMoving;

        if (isDriver && paddlesMoving) {
            return false;
        }

        // Players can't attack passengers of the same vehicle/boat
        if (vehicle.getPassengers().contains(combat.getAttacked())) {
            return false;
        }

        return true;
    }

}
