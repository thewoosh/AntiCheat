package me.thewoosh.anticheat.check.combat;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.*;
import me.thewoosh.anticheat.interactions.MathHelper;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

public class AuraCheck extends Check implements CombatCheck, MoveCheck {

    public AuraCheck() {
        super(Category.COMBAT, "Aura");
    }

    @Override
    public boolean runCheck(@NotNull PlayerData player, @NotNull CombatData combat) {
//        Bukkit.broadcastMessage("§8[§6§lAC§8] §7Attack!");
        return true;
    }

    @Override
    public Result runCheck(@NotNull PlayerData player, @NotNull MovementData movement) {
        final boolean yawChanged = !MathHelper.compareDoubles(movement.getTo().getYaw(), movement.getFrom().getYaw());
        final boolean pitchChanged = !MathHelper.compareDoubles(movement.getTo().getPitch(), movement.getFrom().getPitch());

        if (yawChanged || pitchChanged) {
//            RayTraceResult rdesult = movement.getTo().getWorld().rayTraceEntities(movement.getTo(), movement.getTo().getDirection(), 6);
//            Bukkit.broadcastMessage("§8[§6§lAC§8] §7Rotation change (y=§6%.5f §7p=§6%.5f§7)"/*t=§6%b§7)"*/.formatted(
//                    movement.getTo().getYaw(),
//                    movement.getTo().getPitch()
////                    result != null && result.getHitEntity() != null
//            ));
        }

        return null;
    }
}
