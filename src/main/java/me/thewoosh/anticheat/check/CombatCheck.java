/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check;

import me.thewoosh.anticheat.data.CombatData;
import me.thewoosh.anticheat.data.PlayerData;
import org.jetbrains.annotations.NotNull;

public interface CombatCheck {

    /**
     * Runs the check.
     *
     * @param player The player which performed the movement.
     * @param combat The information collected about the combat action.
     *
     * @return true if the player passed, otherwise false.
     */
    boolean runCheck(@NotNull PlayerData player,
                     @NotNull CombatData combat);

}
