/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.check;

import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface BlockBreakCheck {

    /**
     * Runs the check.
     *
     * @param player The player which broke the block.
     * @param event The information collected about the broken block.
     *
     * @return null if the player passed, non-null if failed.
     */
    @Nullable
    Result runCheck(@NotNull PlayerData player,
                    @NotNull BlockBreakEvent event);

}
