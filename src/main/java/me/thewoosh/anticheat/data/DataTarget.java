package me.thewoosh.anticheat.data;

/**
 * Some -Data types in PlayerData are used by two targets with different
 * purposes. This enum is used as a parameter to some getters, and can be used
 * and extended to fit the required target.
 */
public enum DataTarget {

    AURA,
    SCAFFOLD

}
