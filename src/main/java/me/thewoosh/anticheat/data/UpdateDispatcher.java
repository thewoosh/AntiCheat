package me.thewoosh.anticheat.data;

import me.thewoosh.anticheat.check.AccurateRotationCheck;
import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.MoveCheck;
import me.thewoosh.anticheat.check.Result;
import me.thewoosh.anticheat.interactions.MathHelper;
import me.thewoosh.anticheat.manage.CheckManager;

import org.jetbrains.annotations.NotNull;

public class UpdateDispatcher {

    @NotNull
    private final CheckManager checkManager;

    public UpdateDispatcher(final @NotNull CheckManager checkManager) {
        this.checkManager = checkManager;
    }

    public void dispatchRotationUpdate(final @NotNull PlayerData playerData,
                                       boolean onGround,
                                       float yaw, float pitch) {
        // Only generic round for now:
        dispatchPositionAndRotationUpdate(playerData, new LocationData(playerData.getLastLocation(), onGround, yaw, pitch));
    }

    public void dispatchPositionUpdate(final @NotNull PlayerData playerData,
                                       boolean onGround,
                                       double x, double y, double z) {
        // Only generic round for now:
        dispatchPositionAndRotationUpdate(playerData, new LocationData(playerData.getLastLocation(), onGround, x, y, z));
    }

    public void dispatchPositionAndRotationUpdate(final @NotNull PlayerData playerData,
                                                  final @NotNull LocationData locationData) {
//        System.out.println("Ok!");

        if (MathHelper.compareDoubles(playerData.getLastLocation().getLocation().getYaw(), locationData.getLocation().getYaw()) &&
            MathHelper.compareDoubles(playerData.getLastLocation().getLocation().getPitch(), locationData.getLocation().getPitch())) {
            // only rotation checks
            return;
        }


        final RotationMotionData rotationMotionData = new RotationMotionData(
                playerData.getLastLocation().getLocation().getPitch(),
                playerData.getLastLocation().getLocation().getYaw(),
                locationData.getLocation().getPitch(),
                locationData.getLocation().getYaw()
        );

        for (final AccurateRotationCheck accurateRotationCheck : this.checkManager.getAccurateRotationChecks()) {
            final Check check = (Check) accurateRotationCheck;

            if (!check.isEnabled()) {
                continue;
            }

            final Result result = accurateRotationCheck.runCheck(playerData, rotationMotionData);
            if (result != null) {
                this.checkManager.forwardResult(playerData.getPlayer(), check, result);
                break;
            }
        }

//        postMotionEvent(playerData, movementData, locationData);
        playerData.setLastLocation(locationData);
    }

    private void postMotionEvent(@NotNull final PlayerData playerData,
                                 @NotNull final MovementData movementData,
                                 @NotNull final LocationData locationData) {
        playerData.setLastMovementData(movementData);
        playerData.setLastLocation(locationData);
    }

}
