package me.thewoosh.anticheat.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * (I believe in 1.9) the client no longer sends a packet for when they open
 * their own inventory (CraftingInventory). We'd still like to know if they
 * did, so this wrapper is for that.
 */
public class InventoryData {

    private static final short DELAY_CAP = 1;

    public enum InventoryOpenMotivation {
        /**
         * If the player clicks in their inventory, we know for a fact that it
         * is opened.
         */
        CLICK_EVENT,

        /**
         * If the player adds an item from the creative menu into their
         * inventory.
         */
        CREATIVE_EVENT,

        /**
         * If the player opens their own inventory (if server is < 1.9) or they
         * open an inventory that isn't their own (creative, crafting or
         * spectator).
         */
        OPEN_EVENT,
    }

    /**
     * Why can the inventory be considered 'opened'?
     */
    private InventoryOpenMotivation openMotivation = null;

    /**
     * The client sends a STOP_SNEAK or STOP_SPRINT *after* the inventory is
     * opened, this 'delay' accounts for that.
     */
    private short delay = 0;

    public boolean isOpen() {
        return openMotivation != null;
    }

    public void setClosed() {
        openMotivation = null;
        delay = 0;
    }

    public void setOpen(@NotNull InventoryOpenMotivation motivation) {
        openMotivation = motivation;
    }

    public boolean checkDelay() {
        if (delay < DELAY_CAP) {
            delay++;
            return true;
        }

        return false;
    }

    @Nullable
    public InventoryOpenMotivation getOpenMotivation() {
        return openMotivation;
    }
}
