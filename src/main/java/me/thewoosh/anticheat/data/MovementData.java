/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.data;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class MovementData {

    public static final double EPSILON = 5.96e-08;

    private final LocationData from;
    private final LocationData to;

    private final double deltaX;
    private final double deltaXZ;
    private final double deltaY;
    private final double deltaZ;

    public MovementData(@NotNull final LocationData from,
                        @NotNull final LocationData to) {
        this.from = from;
        this.to = to;

        this.deltaX = to.getLocation().getX() - from.getLocation().getX();
        this.deltaY = to.getLocation().getY() - from.getLocation().getY();
        this.deltaZ = to.getLocation().getZ() - from.getLocation().getZ();
        this.deltaXZ = Math.sqrt(deltaX * deltaX + deltaZ * deltaZ);
    }

    public MovementData(@NotNull final PlayerMoveEvent moveEvent,
                        @NotNull final PlayerData playerData) {
        this(playerData.getLastLocation(), new LocationData(Objects.requireNonNull(moveEvent.getTo()), playerData.wasOnGround()));
    }

    @NotNull
    public LocationData getFromData() {
        return from;
    }

    @NotNull
    public LocationData getToData() {
        return to;
    }

    @NotNull
    public Location getFrom() {
        return from.getLocation();
    }

    @NotNull
    public Location getTo() {
        return to.getLocation();
    }

    public double getDeltaY() {
        return deltaY;
    }

    public double getDeltaX() {
        return deltaX;
    }

    public double getDeltaXZ() {
        return deltaXZ;
    }

    public double getDeltaZ() {
        return deltaZ;
    }

    public boolean isDeltaY(double expected) {
        return Math.abs(deltaY - expected) < EPSILON;
    }

    public boolean wasOnGround() {
        return from.isClientOnGround();
    }

    public boolean isOnGround() {
        return to.isClientOnGround();
    }

}
