package me.thewoosh.anticheat.data;

import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.jetbrains.annotations.NotNull;

public class CombatData {

    private final EntityDamageByEntityEvent event;

    public CombatData(@NotNull EntityDamageByEntityEvent event) {
        this.event = event;
    }

    @NotNull
    public Entity getAttacked() {
        return event.getEntity();
    }

    @NotNull
    public EntityDamageByEntityEvent getEvent() {
        return event;
    }
}
