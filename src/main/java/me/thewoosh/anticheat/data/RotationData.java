package me.thewoosh.anticheat.data;

import me.thewoosh.anticheat.interactions.MathHelper;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class RotationData {

    private static final byte HISTORY_SIZE = 15;

    public static class RotationEntry {
        public float pitch;
        public float yaw;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RotationEntry that = (RotationEntry) o;
            return MathHelper.compareDoubles(that.pitch, this.pitch) &&
                   MathHelper.compareDoubles(that.yaw, this.yaw);
        }

        public boolean elevatedEquals(@NotNull RotationEntry other) {
            return MathHelper.compareDoubles(other.pitch, this.pitch) &&
                    MathHelper.compareDoubles(other.yaw, this.yaw);
        }

        @Override
        public int hashCode() {
            return Objects.hash(pitch, yaw);
        }
    }

    private final RotationEntry[] history = new RotationEntry[HISTORY_SIZE];
    private byte entries = 0;

    public RotationData() {
        for (int i = 0; i < history.length; i++) {
            history[i] = new RotationEntry();
        }
    }

    public RotationEntry addEntry(float pitch, float yaw) {
        if (entries < HISTORY_SIZE)
            entries++;

        for (int i = HISTORY_SIZE - 1; i > 0; i--) {
            history[i].pitch = history[i - 1].pitch;
            history[i].yaw   = history[i - 1].yaw;
        }

        history[0].pitch = pitch;
        history[0].yaw = fixYaw(yaw);

        return history[0];
    }

    private float fixYaw(float yaw) {
        yaw %= 360;
        while (yaw < 0)
            yaw += 360;
        return yaw;
    }

    public boolean isReady() {
        return entries == HISTORY_SIZE;
    }

    public RotationEntry[] getHistory() {
        return history;
    }
}
