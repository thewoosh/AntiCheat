package me.thewoosh.anticheat.data;

public class RotationMotionData {

    public final float fromPitch;
    public final float toPitch;
    public final float deltaPitch;

    public final float deltaYaw;
    public final float fromYaw;
    public final float toYaw;

    public RotationMotionData(final float fromPitch,
                              final float fromYaw,
                              final float toPitch,
                              final float toYaw) {
        this.fromPitch = fromPitch;
        this.fromYaw = fixYaw(fromYaw);
        this.toPitch = toPitch;
        this.toYaw = fixYaw(toYaw);

        this.deltaPitch = this.toPitch - this.fromPitch;
        this.deltaYaw = this.toYaw - this.fromYaw;
    }

    private float fixYaw(float value) {
        value %= 360;
        while (value < 0)
            value += 360;
        return value;
    }

}
