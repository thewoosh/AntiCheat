package me.thewoosh.anticheat.data;

public class DebugData {

    public static final class DebugToggle {

        private boolean value;
        
        public DebugToggle(boolean initialValue) {
            this.value = initialValue;
        }

        public boolean toggle() {
            return value = !value;
        }

        public boolean get() {
            return value;
        }

    }

    public final DebugToggle recordMotion = new DebugToggle(false);
    public final DebugToggle recordPosition = new DebugToggle(false);
    public final DebugToggle recordLadderInteractions = new DebugToggle(false);
    public final DebugToggle recordPlayerPackets = new DebugToggle(false);

}
