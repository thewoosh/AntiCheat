package me.thewoosh.anticheat.data;

import me.thewoosh.anticheat.interactions.MathHelper;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

public class LocationData {

    @NotNull
    private final Location location;
    private final boolean clientOnGround;

    public LocationData(@NotNull final Location location,
                        final boolean clientOnGround) {
        this.location = location;
        this.clientOnGround = clientOnGround;
    }

    public LocationData(LocationData lastLocation, boolean onGround, float yaw, float pitch) {
        Location tempLocation = lastLocation.getLocation().clone();
        tempLocation.setYaw(yaw);
        tempLocation.setPitch(pitch);

        this.location = tempLocation;
        this.clientOnGround = onGround;
    }

    public LocationData(LocationData lastLocation, boolean onGround, double x, double y, double z) {
        Location tempLocation = lastLocation.getLocation().clone();
        tempLocation.setX(x);
        tempLocation.setY(y);
        tempLocation.setZ(z);

        this.location = tempLocation;
        this.clientOnGround = onGround;
    }

    /**
     * Does the client tells us its on ground?
     *
     * @apiNote This value can be spoofed by modified clients
     */
    public boolean isClientOnGround() {
        return clientOnGround;
    }

    @NotNull
    public Location getLocation() {
        return location;
    }

    public boolean equalsRotation(float yaw, float pitch) {
        return MathHelper.compareDoubles(location.getYaw(), yaw) &&
                MathHelper.compareDoubles(location.getPitch(), pitch);
    }

    public boolean equalsPosition(double x, double y, double z) {
        return MathHelper.compareDoubles(location.getX(), x) &&
                MathHelper.compareDoubles(location.getY(), y) &&
                MathHelper.compareDoubles(location.getZ(), z);
    }

}
