/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.data;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class PlayerData {

    private final Player player;
    private MovementData lastMovementData;
    private final BoatData boatData = new BoatData();
    private final DebugData debugData = new DebugData();
    private boolean wasOnGround;
    private final InventoryData inventoryData = new InventoryData();

    private final RotationData rotationDataAura = new RotationData();
    private final RotationData rotationDataScaffold = new RotationData();

    // TODO account for teleports et cetera.
    @NotNull
    private LocationData lastLocation;

    public PlayerData(@NotNull final Player player) {
        this.player = player;

        // Create 0-motion movement
        this.lastLocation = new LocationData(player.getLocation(), player.isOnGround());
        this.lastMovementData = new MovementData(lastLocation, lastLocation);
    }

    @NotNull
    public Player getPlayer() {
        return player;
    }

    @NotNull
    public MovementData getLastMovementData() {
        return lastMovementData;
    }

    public void setLastMovementData(@NotNull MovementData lastMovementData) {
        this.lastMovementData = lastMovementData;
    }

    @NotNull
    public BoatData getBoatData() {
        return boatData;
    }

    @NotNull
    public DebugData getDebugData() {
        return debugData;
    }

    @NotNull
    public InventoryData getInventoryData() {
        return inventoryData;
    }

    @NotNull
    public RotationData getRotationData(@NotNull final DataTarget target) {
        return switch (target) {
            case AURA -> rotationDataAura;
            case SCAFFOLD -> rotationDataScaffold;
        };
    }

    @NotNull
    public LocationData getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(@NotNull LocationData lastLocation) {
        this.lastLocation = lastLocation;
    }

    @NotNull
    public World getWorld() {
        return player.getWorld();
    }

    public void setWasOnGround(boolean onGround) {
        this.wasOnGround = onGround;
    }

    public boolean wasOnGround() {
        return wasOnGround;
    }
}
