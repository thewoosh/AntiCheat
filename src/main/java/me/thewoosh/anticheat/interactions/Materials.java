package me.thewoosh.anticheat.interactions;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

public class Materials {

    public static boolean isChest(@NotNull Block block) {
        return isChest(block.getType());
    }

    public static boolean isChest(@NotNull Material material) {
        return switch (material) {
            case CHEST, ENDER_CHEST, TRAPPED_CHEST -> true;
            default -> false;
        };
    }

}
