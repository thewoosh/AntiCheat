package me.thewoosh.anticheat.interactions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.*;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.block.data.type.TrapDoor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class BlockInteractions {

    private static final HashMap<Material, Double> UNISECTED_HALF_BLOCK_HEIGHTS = new HashMap<>();

    static {
        UNISECTED_HALF_BLOCK_HEIGHTS.put(Material.COMPARATOR, 2 / 16D);
        UNISECTED_HALF_BLOCK_HEIGHTS.put(Material.DAYLIGHT_DETECTOR, 6 / 16D);
        UNISECTED_HALF_BLOCK_HEIGHTS.put(Material.ENCHANTING_TABLE, 12 / 16D);
        UNISECTED_HALF_BLOCK_HEIGHTS.put(Material.END_PORTAL_FRAME, 13 / 16D);
        UNISECTED_HALF_BLOCK_HEIGHTS.put(Material.REPEATER, 2 / 16D);
    }

    private static double getClimbableOffset(@NotNull Location location,
                                             @NotNull BlockFace face) {
        return switch (face) {
            case NORTH -> location.getZ() - location.getBlockZ();
            case SOUTH -> 1 - (location.getZ() - location.getBlockZ());
            case EAST -> 1 - (location.getX() - location.getBlockX());
            case WEST -> location.getX() - location.getBlockX();
            case UP -> 2; // Invalid value to prevent checking
            default -> throw new IllegalStateException("Ladder facing is " + face);
        };
    }

    @NotNull
    private static ClimbInteraction getInteractingLevel(Location location, Set<BlockFace> faces) {
        for (BlockFace face : faces) {
            double offset = getClimbableOffset(location, face);
            if (MathHelper.compareDoubles(offset, 0.5125)) {
                return ClimbInteraction.FULL;
            }
        }

        return ClimbInteraction.SEMI;
    }

    @NotNull
    public static ClimbInteraction findLatterInteraction(@NotNull final Location location,
                                                         @NotNull final BlockFace relativeToLocation) {
        Block block = location.getBlock().getRelative(relativeToLocation);

        if (block.getType() == Material.LADDER) {
            Directional directionalState = (Directional) block.getState().getBlockData();
            return getInteractingLevel(location, Collections.singleton(directionalState.getFacing()));
        }

        if (block.getType() == Material.VINE) {
            MultipleFacing multipleFacing = (MultipleFacing) block.getState().getBlockData();
            return getInteractingLevel(location, multipleFacing.getFaces());
        }

        // TODO Add Scaffolding

        return ClimbInteraction.NOT;
    }

    private static double getNonFullBlockHeight(@NotNull Location location) {
        double highest = 0.0f;

        for (int x = -1; x < 2; x++) {
            for (int z = -1; z < 2; z++) {
                Block block = location.clone().add(x * 0.3, 0, z * 0.3).getBlock();

                // Full blocks
                if (block.getType().isOccluding() && highest < 1.0) {
                    highest = 1.0f;
                    System.out.println("Found full-block at: " + block.getY() + " highest is now " + highest);
                    continue;
                }

                if (block.getState().getBlockData() instanceof Slab && highest < 0.5f) {
                    highest = 0.5F;
                    System.out.println("Found slab at: " + block.getY() + " highest is now " + highest);
                    continue;
                }

                // ....
            }
        }

        return highest;
    }

    public static double getLowestPositionOnLocation(@NotNull Location location) {
        double highest = getNonFullBlockHeight(location);

        if (highest == 0) {
            return highest;
        }

        for (int x = -1; x < 2; x++) {
            for (int z = -1; z < 2; z++) {
                Block block = location.clone().add(x * 0.3, -1, z * 0.3).getBlock();

//                return
            }
        }


        System.out.println("Location=" + location + " block is " + location.getBlock().getType());



        System.out.println("Returning with highest=" + highest);
        return highest;
    }

    public static double calculatePossibleSlabMovement(@NotNull Location from,
                                                       @NotNull Location to) {
//        Bukkit.broadcastMessage("FromY: " + from.getY() + " ToY: " + to.getY());
        Block block = from.getBlock();
        Bukkit.broadcastMessage("Block: " + block.getType() + " is " + block.getBoundingBox().getMax());

        return 0;
    }

    public static HighestResult calculateHeightAt(@NotNull Location location) {
        return calculateHeightAt(location.clone(), 0);
    }

    public static class HighestResult {
        @Nullable
        public Block block = null;
        public double height = 0;

        public void tryCandidate(@NotNull Block block, double height) {
            if (this.block == null || this.height < height) {
                this.block = block;
                this.height = height;
            }
        }

        @Override
        public String toString() {
            return "HighestResult{type=%s,height=%.3f}".formatted(
                    block == null ? "null" : block.getType(),
                    height
            );
        }
    }

    private static HighestResult calculateHeightAt(@NotNull Location location, int tries) {
        Collection<Block> blockCollection = BlockCollections.getBlocksSurrounding(location);
        HighestResult result = new HighestResult();

        for (Block block : blockCollection) {
            final BlockData blockData = block.getState().getBlockData();

//            System.out.println("BlockCandidate> " + Information.withCoordinates(block.getLocation()) + " " + block.getType());

            if (blockData instanceof Slab) {
                Slab slab = (Slab) blockData;
                result.tryCandidate(block, block.getY() + switch (slab.getType()) {
                    case DOUBLE, TOP -> 1.0;
                    case BOTTOM -> 0.5;
                });
                continue;
            }

            Double halfBlockHeight = UNISECTED_HALF_BLOCK_HEIGHTS.get(block.getType());
            if (halfBlockHeight != null) {
                result.tryCandidate(block, block.getY() + halfBlockHeight);
                continue;
            }

            double d;
            double dX = location.getX() - block.getX();
            double dZ = location.getZ() - block.getZ();

            if (block.getType() == Material.END_ROD &&
                    (d = tryEndRodCandidate((Directional)blockData, dX, dZ)) != 0) {
                result.tryCandidate(block, block.getY() + d);
                continue;
            }

            if (blockData instanceof TrapDoor &&
                    (d = tryTrapDoorCandidate((TrapDoor) blockData, dX, dZ)) != 0) {
                result.tryCandidate(block, block.getY() + d);
                continue;
            }

            if (blockData instanceof Stairs &&
                    (d = tryStairsCandidate((Stairs)blockData, dX, dZ)) != 0) {
                result.tryCandidate(block, block.getY() + d);
                continue;
            }

            if (Materials.isChest(block) && tryChestCandidate(block, location)) {
                result.tryCandidate(block, block.getY() + BlockHeights.CHEST_HEIGHT);
                continue;
            }

            if (block.getType().isOccluding()) {
                result.tryCandidate(block, block.getY() + 1.0);
            }

        }

        if (result.block == null || result.height == 0.0) {
            return calculateHeightAt(location.add(0, -1, 0), tries + 1);
        }

        return result;
    }

    private static double tryEndRodCandidate(Directional directional, double dX, double dZ) {
        final double min =  6 / 16.0 - 0.3;
        final double max = 10 / 16.0 + 0.3;
        final double layingHeight = 10 / 16.0;

        return switch (directional.getFacing()) {
            case UP, DOWN -> dX > min && dX < max && dZ > min && dZ < max ? 1 : 0;
            case NORTH, SOUTH -> fullBlockAxisRequisite(dX) && dZ > min && dZ < max ? layingHeight : 0;
            case EAST, WEST -> fullBlockAxisRequisite(dZ) && dX > min && dX < max ? layingHeight : 0;
            default -> 0;
        };
    }

    private static boolean fullBlockAxisRequisite(double value) {
        final double min = -BlockHeights.PLAYER_HWIDTH;
        final double max = BlockHeights.FULL_BLOCK_REQUISITE_MAX;

        return value > min && value < max;
    }

    private static double tryTrapDoorCandidate(TrapDoor trapDoor, double dX, double dZ) {
        if (!trapDoor.isOpen()) {
            return trapDoor.getHalf() == Bisected.Half.TOP ? 1.0 : BlockHeights.TRAP_DOOR_WIDTH;
        }

        return switch (trapDoor.getFacing()) {
            case NORTH -> fullBlockAxisRequisite(dX) && dZ > 0.5125 && dZ < 1.3;
            case EAST -> fullBlockAxisRequisite(dZ) && dX > -0.3 && dX < 0.4875;
            case SOUTH -> fullBlockAxisRequisite(dX) && dZ > -0.3 && dZ < 0.4875;
            case WEST -> fullBlockAxisRequisite(dZ) && dX > 0.5125 && dX < 1.3;
            default -> false;
        } ? 1.0 : 0.0;

    }

    private static double tryStairsCandidate(Stairs stairs, double dX, double dZ) {
        double north = getStairsPos(dZ);
        double east = getStairsNeg(dX);
        double west = getStairsPos(dX);
        double south = getStairsNeg(dZ);

        boolean isInner = stairs.getShape() == Stairs.Shape.INNER_LEFT ||
                          stairs.getShape() == Stairs.Shape.INNER_RIGHT;

        return switch (stairs.getShape()) {
            case STRAIGHT -> switch (stairs.getFacing()) {
                    case NORTH -> north;
                    case EAST -> east;
                    case WEST -> west;
                    case SOUTH -> south;
                    default -> 0.0;
                };
            case OUTER_RIGHT, INNER_RIGHT -> switch (stairs.getFacing()) {
                    case NORTH -> chooseStairsValue(north, east, isInner);
                    case EAST -> chooseStairsValue(east, south, isInner);
                    case WEST -> chooseStairsValue(west, north, isInner);
                    case SOUTH -> chooseStairsValue(south, west, isInner);
                    default -> 0.0;
                };
            case OUTER_LEFT, INNER_LEFT -> switch (stairs.getFacing()) {
                    case NORTH -> chooseStairsValue(north, west, isInner);
                    case EAST -> chooseStairsValue(east, north, isInner);
                    case WEST -> chooseStairsValue(west, south, isInner);
                    case SOUTH -> chooseStairsValue(south, east, isInner);
                    default -> 0.0;
                };
        };
    }

    private static double chooseStairsValue(double candidateA, double candidateB, boolean inner) {
        return inner ?
                Math.max(candidateA, candidateB) :
                Math.min(candidateA, candidateB);
    }

    private static double getStairsNeg(double value) {
        if (value >= 0.2)
            return 1.0;
        if (value >= -0.3)
            return 0.5;
        return 0.0;
    }

    private static double getStairsPos(double value) {
        if (value <= 0.8)
            return 1.0;
        if (value <= 1.3)
            return 0.5;
        return 0.0;
    }

    /**
     * Checks if at (x, blockY and z) the player should be standing on the
     * chest block, or be next to it.
     *
     * @return true if they are on top of the chest
     */
    private static boolean tryChestCandidate(Block block, Location location) {
        // Double-chest interaction isn't a problem, since if both blocks are
        // selected by BlockCollections, either one or both are valid.
        // And if only one is selected, it also still works.

        final double minLim = -0.2375;
        final double maxLim =  1.2375;

        double dX = location.getX() - block.getX();
        double dZ = location.getZ() - block.getZ();

        return dX > minLim && dX < maxLim && dZ > minLim && dZ < maxLim;
    }

}
