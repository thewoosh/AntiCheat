package me.thewoosh.anticheat.interactions;

import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class BlockCollections {

    public static String printBlockCollection(final String tag, final Collection<Block> blocks) {
        StringBuilder builder = new StringBuilder();
        builder.append(tag).append('(').append(blocks.size()).append("): ");

        if (blocks.size() == 0) {
            builder.append("none");
        }

        for (Block block : blocks) {
            builder.append(block.getType().name())
                    .append('(')
                    .append(block.getX()).append(' ')
                    .append(block.getY()).append(' ')
                    .append(block.getZ()).append(')')
                    .append(' ');
        }
        return builder.toString().trim();
    }

    public static Block getIncorrectBlock(final Collection<Block> blocks, final Predicate<Block> predicate) {
        for (Block block : blocks) {
            if (!predicate.test(block)) {
                return block;
            }
        }

        return null;
    }

    public static Collection<Block> getBlocksSurrounding(Location location) {
        double halfWidth = 0.3D;
        ArrayList<Block> blocks = new ArrayList<>();

        for (int x = -1; x < 2; x++) {
            for (int z = -1; z < 2; z++) {
                Block block = location.clone().add(x * halfWidth, 0, z * halfWidth).getBlock();
                if (!blocks.contains(block)) {
                    blocks.add(block);
                }
            }
        }

        return blocks;
    }
}
