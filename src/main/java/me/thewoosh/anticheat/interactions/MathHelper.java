package me.thewoosh.anticheat.interactions;

import me.thewoosh.anticheat.data.MovementData;

public class MathHelper {

    public static boolean compareDoubles(double a, double b) {
        return Math.abs(a - b) <= MovementData.EPSILON;
    }

}
