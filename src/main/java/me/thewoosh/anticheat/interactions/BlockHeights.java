package me.thewoosh.anticheat.interactions;

public class BlockHeights {

    public static final double CHEST_HEIGHT = 0.875;
    public static final double TRAP_DOOR_WIDTH = 3.0 / 16.0;
    
    public static final double PLAYER_WIDTH = 0.6;
    public static final double PLAYER_HWIDTH = PLAYER_WIDTH / 2;

    public static final double FULL_BLOCK_REQUISITE_MAX = 1.0 + BlockHeights.PLAYER_HWIDTH;
}
