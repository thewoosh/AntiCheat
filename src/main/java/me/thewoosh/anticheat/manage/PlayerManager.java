/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.manage;

import me.thewoosh.anticheat.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private final Map<UUID, PlayerData> playerDataMap = new HashMap<>();

    public PlayerManager() {
        checkForPlayers();
    }

    /**
     * Checks for players that already were in the server at ctor.
     * E.g. useful when the server/plugin is reloaded.
     */
    private void checkForPlayers() {
        for (final Player player : Bukkit.getOnlinePlayers()) {
            if (!this.playerDataMap.containsKey(player.getUniqueId())) {
                registerPlayer(player);
            }
        }
    }

    @Nullable
    public PlayerData queryPlayerData(final Player player) {
        return playerDataMap.getOrDefault(player.getUniqueId(), null);
    }

    public void registerPlayer(final Player player) {
        playerDataMap.put(player.getUniqueId(), new PlayerData(player));
    }

    public void unregisterPlayer(final Player player) {
        playerDataMap.remove(player.getUniqueId());
    }

}
