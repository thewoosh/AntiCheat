/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.manage;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.check.block.IllegalBreakCheck;
import me.thewoosh.anticheat.check.block.ScaffoldCheck;
import me.thewoosh.anticheat.check.combat.AuraCheck;
import me.thewoosh.anticheat.check.combat.BoatCombat;
import me.thewoosh.anticheat.check.inventory.IllegalCreativeInventoryCheck;
import me.thewoosh.anticheat.check.move.FlyCheck;
import me.thewoosh.anticheat.check.move.InventoryMoveCheck;
import me.thewoosh.anticheat.check.move.NoFallCheck;
import me.thewoosh.anticheat.check.move.SprintCheck;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CheckManager {

    private final Logger logger;
    private final List<Check> checks;
    private final List<AccurateRotationCheck> accurateRotationChecks;
    private final List<BlockBreakCheck> blockBreakChecks;
    private final List<BlockPlaceCheck> blockPlaceChecks;
    private final List<CombatCheck> combatChecks;
    private final List<InventoryCheck> inventoryChecks;
    private final List<MoveCheck> movementChecks;

    public CheckManager(final Logger logger) {
        this.logger = logger;
        this.checks = new ArrayList<>();
        this.accurateRotationChecks = new ArrayList<>();
        this.blockBreakChecks = new ArrayList<>();
        this.blockPlaceChecks = new ArrayList<>();
        this.inventoryChecks = new ArrayList<>();
        this.movementChecks = new ArrayList<>();
        this.combatChecks = new ArrayList<>();

        // Block Checks
        addCheck(new IllegalBreakCheck());
        addCheck(new ScaffoldCheck());

        // Combat Checks
        addCheck(new AuraCheck());
        addCheck(new BoatCombat());

        // Inventory Checks
        addCheck(new IllegalCreativeInventoryCheck());

        // Movement Checks
        addCheck(new SprintCheck());
//        addCheck(new FlyCheck());
        addCheck(new InventoryMoveCheck());
//        addCheck(new NoFallCheck());
    }

    private void addCheck(Check check) {
        checks.add(check);
        boolean wasRecognizedOnce = false;

        if (check instanceof AccurateRotationCheck) {
            accurateRotationChecks.add((AccurateRotationCheck) check);
            wasRecognizedOnce = true;
        }

        if (check instanceof BlockBreakCheck) {
            blockBreakChecks.add((BlockBreakCheck) check);
            wasRecognizedOnce = true;
        }

        if (check instanceof BlockPlaceCheck) {
            blockPlaceChecks.add((BlockPlaceCheck) check);
            wasRecognizedOnce = true;
        }

        if (check instanceof CombatCheck) {
            combatChecks.add((CombatCheck) check);
            wasRecognizedOnce = true;
        }

        if (check instanceof InventoryCheck) {
            inventoryChecks.add((InventoryCheck) check);
            wasRecognizedOnce = true;
        }

        if (check instanceof MoveCheck) {
            movementChecks.add((MoveCheck) check);
            wasRecognizedOnce = true;
        }

        if (!wasRecognizedOnce) {
            this.logger.warning("Check \"" + check.getName() + "\" with category \"" + check.getCategory() + "\" wasn't recognized as any implementer of Check-related interface!");
        }
    }


    /**
     * In production, this function should/let handle the VL, warnings, kicks etc.
     */
    public void forwardResult(@NotNull final Player player,
                              @NotNull final Check check,
                              @NotNull final Result result) {
        Bukkit.broadcastMessage("§8[§6§lAC§8] §6%s §7(§6%s§7) failed for §6%s".formatted(
                check.getName(),
                result.getSubCheckName(),
                player.getName()
        ));

        for (String string : result.getVerboseInformation()) {
            this.logger.info("[Verbose] " + string);
        }
    }

    public List<Check> getChecks() {
        return checks;
    }

    public List<AccurateRotationCheck> getAccurateRotationChecks() {
        return accurateRotationChecks;
    }

    public List<BlockBreakCheck> getBlockBreakChecks() {
        return blockBreakChecks;
    }

    public List<BlockPlaceCheck> getBlockPlaceChecks() {
        return blockPlaceChecks;
    }

    public List<CombatCheck> getCombatChecks() {
        return combatChecks;
    }

    public List<InventoryCheck> getInventoryChecks() { return inventoryChecks; }

    public List<MoveCheck> getMovementChecks() {
        return movementChecks;
    }

}
