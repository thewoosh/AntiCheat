package me.thewoosh.anticheat.commands;

import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.interactions.BlockInteractions;
import me.thewoosh.anticheat.manage.PlayerManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class CommandAntiCheat implements CommandExecutor, TabCompleter {

    private final HashMap<String, Function<@NotNull SubCommandArguments, Boolean>> subCommands = new HashMap<>();
    private final PlayerManager playerManager;

    public CommandAntiCheat(@NotNull final PlayerManager playerManager) {
        this.playerManager = playerManager;

        subCommands.put("record-ladder-interactions", CommandAntiCheat::recordLadderInteractions);
        subCommands.put("record-motion", CommandAntiCheat::recordMotion);
        subCommands.put("record-position", CommandAntiCheat::recordPosition);
        subCommands.put("record-player-packets", CommandAntiCheat::recordPlayerPackets);
        subCommands.put("collision", CommandAntiCheat::collision);
        subCommands.put("what-block", CommandAntiCheat::whatBlock);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender,
                             @NotNull Command command,
                             @NotNull String label,
                             String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        if (args.length == 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Usage: /ac <command>.\nList of subcommands:");

            int i = 0;
            for (String string : subCommands.keySet()) {
                stringBuilder.append(++i)
                        .append(". ")
                        .append(string)
                        .append('\n');
            }

            sender.sendMessage(stringBuilder.toString());
            return true;
        }

        Function<@NotNull SubCommandArguments, Boolean> function = subCommands.get(args[0].toLowerCase());
        if (function == null) {
            sender.sendMessage("No subcommand found with name: '" + args[0].toLowerCase() + "'. Use '/ac' to see a list of subcommands.");
            return true;
        }

        PlayerData playerData = playerManager.queryPlayerData((Player) sender);
        if (playerData == null) {
            sender.sendMessage("No registry in PlayerManager");
            return true;
        }

        return function.apply(new SubCommandArguments(playerData, args));
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender,
                                      @NotNull Command command,
                                      @NotNull String alias,
                                      String[] args) {
        if (args.length != 1) {
            return new ArrayList<>();
        }

        ArrayList<String> list = new ArrayList<>();
        String input = args[0].toLowerCase();

        for (String subCommand : subCommands.keySet()) {
            if (subCommand.startsWith(input)) {
                list.add(subCommand);
            }
        }

        return list;
    }

    public static boolean recordPosition(@NotNull SubCommandArguments args) {
        boolean toggled = args.getPlayerData().getDebugData().recordPosition.toggle();
        args.getPlayerData().getPlayer().sendMessage("[Record-Position] Now: " + (toggled ? "recording" : "not recording"));
        return true;
    }

    public static boolean recordPlayerPackets(@NotNull SubCommandArguments args) {
        boolean toggled = args.getPlayerData().getDebugData().recordPlayerPackets.toggle();
        args.getPlayerData().getPlayer().sendMessage("[Record-Player-Packets] Now: " + (toggled ? "recording" : "not recording"));
        return true;
    }

    public static boolean recordMotion(@NotNull SubCommandArguments args) {
        boolean toggled = args.getPlayerData().getDebugData().recordMotion.toggle();
        args.getPlayerData().getPlayer().sendMessage("[Record-Motion] Now: " + (toggled ? "recording" : "not recording"));
        return true;
    }

    public static boolean recordLadderInteractions(@NotNull SubCommandArguments args) {
        boolean toggled = args.getPlayerData().getDebugData().recordLadderInteractions.toggle();
        args.getPlayerData().getPlayer().sendMessage("[Record-Ladder-Interactions] Now: " + (toggled ? "recording" : "not recording"));
        return true;
    }

    private static boolean collision(@NotNull SubCommandArguments subCommandArguments) {
        BlockInteractions.getLowestPositionOnLocation(subCommandArguments.getPlayerData().getPlayer().getLocation());

        return true;
    }

    private static boolean whatBlock(@NotNull SubCommandArguments subCommandArguments) {
        Player player = subCommandArguments.getPlayerData().getPlayer();
        BlockInteractions.HighestResult result = BlockInteractions.calculateHeightAt(player.getLocation());
        player.sendMessage("§8[§6§lAC§8] §7You're standing on §6%s §7with y=§6%.5f§7.".formatted(
                (result.block == null ? "null" : result.block.getType().name()),
                result.height
        ));

        return true;
    }

}
