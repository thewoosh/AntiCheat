package me.thewoosh.anticheat.commands;

import me.thewoosh.anticheat.data.PlayerData;
import org.jetbrains.annotations.NotNull;

class SubCommandArguments {

    private final PlayerData player;
    private final String[] args;

    public SubCommandArguments(@NotNull PlayerData player,
                               @NotNull String[] args) {
        this.player = player;
        this.args = args;
    }

    @NotNull
    public PlayerData getPlayerData() {
        return player;
    }

    @NotNull
    public String[] getArgs() {
        return args;
    }

}
