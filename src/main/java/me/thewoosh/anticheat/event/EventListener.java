/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package me.thewoosh.anticheat.event;

import me.thewoosh.anticheat.check.*;
import me.thewoosh.anticheat.data.CombatData;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.manage.CheckManager;
import me.thewoosh.anticheat.manage.PlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

public class EventListener implements Listener {

    @NotNull
    private final CheckManager checkManager;

    @NotNull
    private final Logger logger;

    @NotNull
    private final PlayerManager playerManager;

    public EventListener(@NotNull final CheckManager checkManager,
                         @NotNull final Logger logger,
                         @NotNull final PlayerManager playerManager) {
        this.checkManager = checkManager;
        this.logger = logger;
        this.playerManager = playerManager;
    }


    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this.playerManager.registerPlayer(event.getPlayer());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        final PlayerData playerData = this.playerManager.queryPlayerData(event.getPlayer());

        if (playerData == null) {
            this.logger.warning("[PlayerMove] Player \"" + event.getPlayer().getName() + "\" isn't registered in PlayerManager");
            return;
        }

        final MovementData movementData = new MovementData(event, playerData);

        for (final MoveCheck moveCheck : this.checkManager.getMovementChecks()) {
            final Check check = (Check) moveCheck;

            if (!check.isEnabled()) {
                continue;
            }

            final Result result = moveCheck.runCheck(playerData, movementData);
            if (result != null) {
                this.checkManager.forwardResult(event.getPlayer(), check, result);
                break;
            }
        }

        playerData.setLastMovementData(movementData);
        playerData.setLastLocation(movementData.getToData());
        playerData.setWasOnGround(event.getPlayer().isOnGround());
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player))
            return;

        PlayerData playerData = this.playerManager.queryPlayerData((Player)event.getDamager());
        if (playerData == null)
            return;

        CombatData combatData = new CombatData(event);

        for (final CombatCheck combatCheck : this.checkManager.getCombatChecks()) {
            final Check check = (Check) combatCheck;
            if (check.isEnabled() && !combatCheck.runCheck(playerData, combatData)) {
                this.logger.warning("[PlayerMove] Check \"" + check.getName() + "\" failed for player \"" + playerData.getPlayer().getName() + '"');
                return;
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        PlayerData playerData = this.playerManager.queryPlayerData(event.getPlayer());
        if (playerData == null)
            return;

        for (final BlockBreakCheck blockBreakCheck : this.checkManager.getBlockBreakChecks()) {
            final Check check = (Check) blockBreakCheck;
            if (!check.isEnabled()) {
                continue;
            }

            Result result = blockBreakCheck.runCheck(playerData, event);
            if (result != null) {
                event.setCancelled(true); // revert breaking.
                this.checkManager.forwardResult(event.getPlayer(), check, result);
                return;
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        PlayerData playerData = this.playerManager.queryPlayerData(event.getPlayer());
        if (playerData == null)
            return;

        for (final BlockPlaceCheck blockPlaceCheck : this.checkManager.getBlockPlaceChecks()) {
            final Check check = (Check) blockPlaceCheck;
            if (!check.isEnabled()) {
                continue;
            }

            Result result = blockPlaceCheck.runCheck(playerData, event);
            if (result != null) {
                this.checkManager.forwardResult(event.getPlayer(), check, result);
                return;
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        this.playerManager.unregisterPlayer(event.getPlayer());
    }

}
