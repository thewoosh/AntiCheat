package me.thewoosh.anticheat.event;

import me.thewoosh.anticheat.data.LocationData;
import me.thewoosh.anticheat.data.MovementData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.interactions.BlockInteractions;
import me.thewoosh.anticheat.manage.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.logging.Logger;

public class DebugListener implements Listener {

    private final Logger logger;
    private final PlayerManager playerManager;

    public DebugListener(@NotNull final Logger logger,
                         @NotNull final PlayerManager playerManager) {
        this.logger = logger;
        this.playerManager = playerManager;
    }

    @NotNull
    private String ladderInteraction(@Nullable String tag,
                                     @NotNull Location location,
                                     @NotNull BlockFace face) {
        Block block = location.getBlock().getRelative(face);
        return tag + "(" + BlockInteractions.findLatterInteraction(location, face) +
                " y=" + block.getY() +
                " m=" + block.getType() +
        ")";
    }


    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        final PlayerData playerData = this.playerManager.queryPlayerData(event.getPlayer());

        if (playerData == null) {
            this.logger.warning("[PlayerMove] Player \"" + event.getPlayer().getName() + "\" isn't registered in PlayerManager");
            return;
        }

        assert event.getTo() != null;

        if (playerData.getDebugData().recordLadderInteractions.get()) {
            Bukkit.broadcastMessage("[Ladder] " +
                    ladderInteraction( "0", event.getFrom(), BlockFace.SELF) +
                    ladderInteraction("+1", event.getFrom(), BlockFace.UP)
            );
        }

        if (playerData.getDebugData().recordPosition.get()) {
            logger.info(String.format("[Position] From(x=%.5f y=%.5f z=%.5f) To(x=%.5f y=%.5f z=%.5f)",
                    event.getFrom().getX(),
                    event.getFrom().getY(),
                    event.getFrom().getZ(),
                    event.getTo().getX(),
                    event.getTo().getY(),
                    event.getTo().getZ()
            ));
        }

        if (playerData.getDebugData().recordMotion.get()) {
            final MovementData movementData = new MovementData(playerData.getLastLocation(), new LocationData(event.getTo(), playerData.getPlayer().isOnGround()));

            logger.info(String.format("[Motion] onGround=%b dx=%.5f dy=%.5f dz=%.5f dh=%.5f",
                    movementData.isOnGround(),
                    movementData.getDeltaX(),
                    movementData.getDeltaY(),
                    movementData.getDeltaZ(),
                    movementData.getDeltaXZ()
            ));
        }
    }

}
