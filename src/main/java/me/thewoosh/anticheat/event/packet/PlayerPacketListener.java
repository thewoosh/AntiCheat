package me.thewoosh.anticheat.event.packet;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;

import me.thewoosh.anticheat.PluginCore;
import me.thewoosh.anticheat.data.LocationData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.data.UpdateDispatcher;
import me.thewoosh.anticheat.manage.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

public class PlayerPacketListener extends PacketAdapter {

    private final PlayerManager playerManager;
    private final UpdateDispatcher updateDispatcher;

    private final PluginCore pluginCore;

    public PlayerPacketListener(@NotNull final PluginCore pluginCore) {
        super(pluginCore,
                PacketType.Play.Client.POSITION,
                PacketType.Play.Client.POSITION_LOOK,
                PacketType.Play.Client.LOOK
        );

        this.pluginCore = pluginCore;

        this.playerManager = pluginCore.getPlayerManager();
        this.updateDispatcher = pluginCore.getUpdateDispatcher();
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        final PlayerData playerData = playerManager.queryPlayerData(event.getPlayer());

        if (playerData == null) {
            return;
        }

        StructureModifier<Double> doubles = event.getPacket().getDoubles();
        StructureModifier<Float> floats = event.getPacket().getFloat();

        boolean onGround = event.getPacket().getBooleans().read(0);

        if (event.getPacketType() == PacketType.Play.Client.POSITION_LOOK) {
            updatePositionRotation(playerData, doubles, floats, onGround);
        } else if (event.getPacketType() == PacketType.Play.Client.POSITION) {
            updatePosition(playerData, doubles, onGround);
        } else if (event.getPacketType() == PacketType.Play.Client.LOOK) {
            updateRotation(playerData, floats, onGround);
        } else {
            Bukkit.broadcastMessage("Unrecoginzed player packet: " + event.getPacketType());
        }
    }

    private void updatePositionRotation(final @NotNull PlayerData playerData,
                                        final @NotNull StructureModifier<Double> doubles,
                                        final @NotNull StructureModifier<Float> floats,
                                        final boolean onGround) {
        final float yaw = floats.read(0);
        final float pitch = floats.read(1);

        final double x = doubles.read(0);
        final double y = doubles.read(1);
        final double z = doubles.read(2);

        if (!playerData.getLastLocation().equalsPosition(x, y, z) &&
            !playerData.getLastLocation().equalsRotation(yaw, pitch)) {
            Bukkit.getScheduler().runTask(pluginCore, () -> updateDispatcher.dispatchPositionAndRotationUpdate(playerData, new LocationData(
                    new Location(playerData.getWorld(), x, y, z, yaw, pitch),
                    onGround
            )));
        }
    }

    private void updatePosition(final @NotNull PlayerData playerData,
                                final @NotNull StructureModifier<Double> doubles,
                                final boolean onGround) {
        final double x = doubles.read(0);
        final double y = doubles.read(1);
        final double z = doubles.read(2);

        if (!playerData.getLastLocation().equalsPosition(x, y, z)) {
            Bukkit.getScheduler().runTask(pluginCore, () -> updateDispatcher.dispatchPositionUpdate(playerData, onGround, x, y, z));
        }
    }

    private void updateRotation(final @NotNull PlayerData playerData,
                                final @NotNull StructureModifier<Float> floats,
                                final boolean onGround) {
        final float yaw = floats.read(0);
        final float pitch = floats.read(1);

        if (!playerData.getLastLocation().equalsRotation(yaw, pitch)) {
            Bukkit.getScheduler().runTask(pluginCore, () -> updateDispatcher.dispatchRotationUpdate(playerData, onGround, yaw, pitch));
        }
    }

}
