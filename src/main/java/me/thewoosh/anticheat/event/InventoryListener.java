package me.thewoosh.anticheat.event;

import me.thewoosh.anticheat.check.Check;
import me.thewoosh.anticheat.check.InventoryCheck;
import me.thewoosh.anticheat.check.Result;
import me.thewoosh.anticheat.data.InventoryData;
import me.thewoosh.anticheat.data.PlayerData;
import me.thewoosh.anticheat.manage.CheckManager;
import me.thewoosh.anticheat.manage.PlayerManager;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.jetbrains.annotations.NotNull;

public class InventoryListener implements Listener {

    private final CheckManager checkManager;
    private final PlayerManager playerManager;

    public InventoryListener(CheckManager checkManager, PlayerManager playerManager) {
        this.checkManager = checkManager;
        this.playerManager = playerManager;
    }

    private void callInventoryCheckCycle(@NotNull InventoryEvent event,
                                         @NotNull HumanEntity humanEntity,
                                         @NotNull InventoryData.InventoryOpenMotivation motivation,
                                         boolean priority) {
        Player player = (Player) humanEntity;
        PlayerData playerData = this.playerManager.queryPlayerData(player);
        if (playerData == null) {
            return;
        }

        InventoryData inventoryData = playerData.getInventoryData();
        if (priority || !inventoryData.isOpen()) {
            inventoryData.setOpen(motivation);
        }

        fireCheckCycle(player, event);
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        callInventoryCheckCycle(event, event.getPlayer(), InventoryData.InventoryOpenMotivation.OPEN_EVENT, true);
    }

    @EventHandler
    public void onInteract(InventoryClickEvent event) {
        callInventoryCheckCycle(event, event.getWhoClicked(), InventoryData.InventoryOpenMotivation.CLICK_EVENT, false);
    }

    @EventHandler
    public void onCreativeAction(InventoryCreativeEvent event) {
        callInventoryCheckCycle(event, event.getWhoClicked(), InventoryData.InventoryOpenMotivation.CREATIVE_EVENT, false);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        PlayerData playerData = playerManager.queryPlayerData((Player) event.getPlayer());
        if (playerData == null)
            return;
        playerData.getInventoryData().setClosed();
        fireCheckCycle(((Player) event.getPlayer()), event);
    }

    private void fireCheckCycle(@NotNull Player player,
                                @NotNull InventoryEvent event) {
        PlayerData playerData = this.playerManager.queryPlayerData(player);

        if (playerData == null) {
            // When the player disconnects, events like InventoryCloseEvent may
            // fire after PlayerQuitEvent
            return;
        }

        for (final InventoryCheck inventoryCheck : this.checkManager.getInventoryChecks()) {
            final Check check = (Check) inventoryCheck;
            if (!check.isEnabled()) {
                continue;
            }

            Result result = inventoryCheck.runCheck(playerData, event);
            if (result != null) {
                this.checkManager.forwardResult(player, check, result);
                break;
            }
        }
    }

}
