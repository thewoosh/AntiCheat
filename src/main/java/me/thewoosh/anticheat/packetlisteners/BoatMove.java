package me.thewoosh.anticheat.packetlisteners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import me.thewoosh.anticheat.PluginCore;
import me.thewoosh.anticheat.data.BoatData;
import me.thewoosh.anticheat.data.PlayerData;

public class BoatMove extends PacketAdapter {

    private final PluginCore pluginCore;

    public BoatMove(PluginCore pluginCore) {
        super(pluginCore, PacketType.Play.Client.BOAT_MOVE);
        this.pluginCore = pluginCore;
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        PlayerData player = pluginCore.getPlayerManager().queryPlayerData(event.getPlayer());
        if (player == null) {
            return;
        }

        BoatData boat = player.getBoatData();

        boolean leftPaddle = event.getPacket().getBooleans().read(0);
        boolean rightPaddle = event.getPacket().getBooleans().read(1);

        boat.leftPaddleMoving = leftPaddle;
        boat.rightPaddleMoving = rightPaddle;
    }
}
